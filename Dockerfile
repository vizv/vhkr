FROM docker.io/library/alpine:3.15.0 as binary

RUN wget -qO- 'https://github.com/k0sproject/k0s/releases/download/v1.23.1+k0s.0/k0s-v1.23.1+k0s.0-amd64' > /usr/local/bin/k0s
RUN wget -qO- 'https://get.helm.sh/helm-v3.7.2-linux-amd64.tar.gz' \
    | tar zx --strip-components=1 --directory=/usr/local/bin linux-amd64/helm
RUN wget -qO- 'https://github.com/kubernetes-sigs/cri-tools/releases/download/v1.22.0/crictl-v1.22.0-linux-amd64.tar.gz' \
    | tar zx --directory=/usr/local/bin crictl
RUN wget -qO- 'https://dl.k8s.io/release/v1.23.1/bin/linux/amd64/kubectl' > /usr/local/bin/kubectl

RUN chmod -v +x /usr/local/bin/*


FROM docker.io/library/alpine:3.15.0

COPY --from=binary /usr/local/bin/ /usr/local/bin/
RUN apk add --no-cache tini

ENV CONTAINER_RUNTIME_ENDPOINT=unix:///run/k0s/containerd.sock \
    KUBECONFIG=/var/lib/k0s/pki/admin.conf

COPY /entrypoint.sh /

ENTRYPOINT ["/sbin/tini", "--", "/bin/sh", "/entrypoint.sh"]
CMD ["k0s", "controller", "--config=/etc/k0s/config.yaml", "--enable-worker", "--no-taints"]

COPY /config-template.yaml /config-template.yaml
