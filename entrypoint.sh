#!/bin/sh -ex

render() { sh -c "echo \"$(cat "$1" | sed 's/["\]/\\&/g')\""; }

mkdir -p /etc/k0s
render /config-template.yaml >/etc/k0s/config.yaml

[ -z "$@" ] && exec /bin/sh
exec "$@"
