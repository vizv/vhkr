# Viz Homelab [k0s](https://k0sproject.io/) Release

## Version

| Item         | Version       | Link                                       |
|--------------|---------------|--------------------------------------------|
| Alpine Linux | 3.15.0        | https://alpinelinux.org/downloads/         |
| k0s          | v1.23.1+k0s.0 | https://github.com/k0sproject/k0s/releases |

# Reference

* [k0s Dockerfile](https://github.com/k0sproject/k0s/blob/main/Dockerfile)
