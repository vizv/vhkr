#!/bin/bash -e

: "${IMAGE_LABEL:=test}"

fail() { echo "[!] FAIL: $*" >&2; }
info() { echo "[+] INFO: $*" >&2; }

info "builing vhkr:${IMAGE_LABEL}..."
docker build -t "vhkr:${IMAGE_LABEL}" .
